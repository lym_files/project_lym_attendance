package main

import (
	controller2 "Time_Attendance_Management_System/static/GOFiles"
	"database/sql"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

var db *sql.DB

func main() {

	router := mux.NewRouter()

	staticDir := "/static/"
	router.PathPrefix(staticDir).Handler(http.StripPrefix(staticDir, http.FileServer(http.Dir("static"))))

	// Define a route for the log.js file

	router.HandleFunc("/static/JScript/log.js", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "static/JScript/*.js")
	})

	router.HandleFunc("/static/css/styles.css", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "static/css/*.css")
	})

	// UserRouter
	router.HandleFunc("/view", controller2.ViewUserHandler).Methods("GET")
	router.HandleFunc("/add", controller2.CreateUserHandler).Methods("POST")
	router.HandleFunc("/update", controller2.UpdateUserHandler).Methods("POST")
	router.HandleFunc("/delete", controller2.DeleteUserHandler).Methods("POST")

	// Method Registration
	router.HandleFunc("/methodreg", controller2.MethodRegistrationHandler).Methods("GET")
	//router.HandleFunc("/pinreg", controller.PinRegistrationHandler).Methods("POST")

	// Input Attended Time and Date
	router.HandleFunc("/viewLogs", controller2.ViewLogsHandler).Methods("GET")
	router.HandleFunc("/attended", controller2.AttendedHandler).Methods("POST")

	// Attendance
	router.HandleFunc("/attendanceview", controller2.AttendanceViewHandler).Methods("GET")
	router.HandleFunc("/search", controller2.AttendanceSearchHandler).Methods("GET")
	router.HandleFunc("/searchperiod", controller2.AttendanceSearchPeriodHandler).Methods("GET")
	router.HandleFunc("/attendPdf", controller2.AttendancePDFHandler).Methods("GET")

	// Summarize
	router.HandleFunc("/summarizeview", controller2.SummarizeViewHandler).Methods("GET")
	router.HandleFunc("/summarizeSearch", controller2.SummarizeSearchHandler).Methods("GET")

	router.HandleFunc("/", controller2.ViewHome).Methods("GET")

	fmt.Println("Server started on http://localhost:3000")
	log.Fatal(http.ListenAndServe(":3000", router))
}
