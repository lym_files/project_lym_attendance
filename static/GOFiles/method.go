package controller

import (
	"html/template"
	_ "modernc.org/sqlite"
	"net/http"
)

func MethodRegistrationHandler(w http.ResponseWriter, r *http.Request) {

	tmpl, err := template.ParseFiles("./static/HTMLFiles/methodUser.html")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
