package controller

import (
	"database/sql"
	"github.com/jung-kurt/gofpdf"
	"html/template"
	"log"
	"net/http"
	"strconv"
	"strings"
)

type Attendance struct {
	ID           int
	UserId       int
	Name         string
	Mode         string
	AttendedDate string
	AttendedTime string
	Method       string
}

func AttendanceViewHandler(w http.ResponseWriter, r *http.Request) {
	db, err := sql.Open("sqlite", "SQLite_TAMS.db")
	if err != nil {
		log.Println("Failed to open database:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	defer db.Close()

	if err = db.Ping(); err != nil {
		log.Println("Failed to connect to the database:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	rows, err := db.Query("SELECT * FROM attendanceTable ORDER BY attendeddate DESC , attendedtime DESC")
	if err != nil {
		log.Println("Failed to execute query:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	defer rows.Close()

	var attendanceDataList []Attendance

	for rows.Next() {
		var attendanceData Attendance
		err := rows.Scan(
			&attendanceData.ID,
			&attendanceData.UserId,
			&attendanceData.Name,
			&attendanceData.Mode,
			&attendanceData.AttendedDate,
			&attendanceData.AttendedTime,
			&attendanceData.Method,
		)
		if err != nil {
			log.Println("Failed to scan row:", err)
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}
		attendanceDataList = append(attendanceDataList, attendanceData)
	}

	if err = rows.Err(); err != nil {
		log.Println("Failed to retrieve rows:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// Pagination logic
	pageStr := r.URL.Query().Get("page")
	page, _ := strconv.Atoi(pageStr)

	if page <= 0 {
		page = 1
	}

	viewsPerPage := 5
	startIndex := (page - 1) * viewsPerPage
	endIndex := startIndex + viewsPerPage

	var pagedViews []Attendance
	if startIndex >= 0 && startIndex < len(attendanceDataList) {
		if endIndex > len(attendanceDataList) {
			endIndex = len(attendanceDataList)
		}
		pagedViews = attendanceDataList[startIndex:endIndex]
	}

	totalPages := len(attendanceDataList) / viewsPerPage
	if len(attendanceDataList)%viewsPerPage != 0 {
		totalPages++
	}

	pages := make([]int, totalPages)
	for i := 0; i < totalPages; i++ {
		pages[i] = i + 1
	}

	data := struct {
		Views      []Attendance
		TotalPages int
		Pages      []int
	}{
		Views:      pagedViews,
		TotalPages: totalPages,
		Pages:      pages,
	}

	tmpl, err := template.ParseFiles("./static/HTMLFiles/attendTable.html")
	if err != nil {
		log.Println("Failed to parse template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, data)
	if err != nil {
		log.Println("Failed to execute template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}
func AttendanceSearchHandler(w http.ResponseWriter, r *http.Request) {
	var err error
	db, err = sql.Open("sqlite", "SQLite_TAMS.db")
	if err != nil {
		log.Println("open Err: ", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	defer db.Close()

	searchKey := "%" + r.FormValue("searchKey") + "%"
	rowValve := "SELECT * FROM attendanceTable " +
		"WHERE userId LIKE ? " +
		"OR name LIKE ? " +
		"OR mode LIKE ? " +
		"OR method LIKE ? ORDER BY attendeddate DESC, attendedtime DESC "
	allRows, err := db.Query(rowValve, searchKey, searchKey, searchKey, searchKey)
	if err != nil {
		log.Println("query Err: ", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	defer allRows.Close()

	var attendViews []Attendance
	for allRows.Next() {
		var attendView Attendance
		err := allRows.Scan(&attendView.ID, &attendView.UserId, &attendView.Name,
			&attendView.Mode, &attendView.AttendedDate, &attendView.AttendedTime, &attendView.Method)
		if err != nil {
			log.Println("scan Err: ", err)
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}
		attendViews = append(attendViews, attendView)
	}

	pageStr := r.URL.Query().Get("page")
	page, _ := strconv.Atoi(pageStr)

	if page <= 0 {
		page = 1
	}

	viewsPerPage := 5
	startIndex := (page - 1) * viewsPerPage
	endIndex := startIndex + viewsPerPage

	var pagedViews []Attendance
	if startIndex >= 0 && startIndex < len(attendViews) {
		if endIndex > len(attendViews) {
			endIndex = len(attendViews)
		}
		pagedViews = attendViews[startIndex:endIndex]
	}

	totalPages := len(attendViews) / viewsPerPage
	if len(attendViews)%viewsPerPage != 0 {
		totalPages++
	}

	pages := make([]int, totalPages)
	for i := 0; i < totalPages; i++ {
		pages[i] = i + 1
	}

	data := struct {
		Views      []Attendance
		TotalPages int
		Pages      []int
	}{
		Views:      pagedViews,
		TotalPages: totalPages,
		Pages:      pages,
	}

	tmpl, err := template.ParseFiles("./static/HTMLFiles/attendTable.html")
	if err != nil {
		log.Println("parseFiles Err: ", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, data)
	if err != nil {
		log.Println("execute Err: ", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

func AttendanceSearchPeriodHandler(w http.ResponseWriter, r *http.Request) {
	var err error
	db, err = sql.Open("sqlite", "SQLite_TAMS.db")
	if err != nil {
		log.Println("open Err: ", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	defer db.Close()

	dateStart := r.FormValue("dateStart")
	dateEnd := r.FormValue("dateEnd")
	searchKey := r.FormValue("searchKey")

	rowValve := "SELECT * FROM attendanceTable " +
		"WHERE (userId LIKE ? OR name LIKE ? OR mode LIKE ? OR method LIKE ?) " +
		"AND attendedDate BETWEEN ? AND ?"
	allRows, err := db.Query(rowValve, "%"+searchKey+"%", "%"+searchKey+"%", "%"+searchKey+"%", "%"+searchKey+"%", dateStart, dateEnd)
	if err != nil {
		log.Println("query Err: ", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	defer allRows.Close()

	var attendViews []Attendance
	for allRows.Next() {
		var attendView Attendance
		err := allRows.Scan(&attendView.ID, &attendView.UserId, &attendView.Name, &attendView.Mode, &attendView.AttendedDate, &attendView.AttendedTime, &attendView.Method)
		if err != nil {
			log.Println("scan Err: ", err)
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}
		attendViews = append(attendViews, attendView)
	}

	// Pagination logic
	pageStr := r.URL.Query().Get("page")
	page, _ := strconv.Atoi(pageStr)

	if page <= 0 {
		page = 1
	}

	viewsPerPage := 5
	startIndex := (page - 1) * viewsPerPage
	endIndex := startIndex + viewsPerPage

	var pagedViews []Attendance
	if startIndex >= 0 && startIndex < len(attendViews) {
		if endIndex > len(attendViews) {
			endIndex = len(attendViews)
		}
		pagedViews = attendViews[startIndex:endIndex]
	}

	totalPages := len(attendViews) / viewsPerPage
	if len(attendViews)%viewsPerPage != 0 {
		totalPages++
	}

	pages := make([]int, totalPages)
	for i := 0; i < totalPages; i++ {
		pages[i] = i + 1
	}

	data := struct {
		Views      []Attendance
		TotalPages int
		Pages      []int
	}{
		Views:      pagedViews,
		TotalPages: totalPages,
		Pages:      pages,
	}

	tmpl, err := template.ParseFiles("./static/HTMLFiles/attendTable.html")
	if err != nil {
		log.Println("parseFiles Err: ", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, data)
	if err != nil {
		log.Println("execute Err: ", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

func AttendancePDFHandler(w http.ResponseWriter, r *http.Request) {
	values := r.URL.Query().Get("all")

	pdf := gofpdf.New("P", "mm", "A4", "")
	pdf.AddPage()

	pdf.SetFont("Arial", "B", 12)
	pdf.SetFillColor(200, 200, 200)

	headers := []string{"UserId", "Name", "Mode", "AttendDate", "AttendTime", "Method"}
	cellWidths := []float64{20, 40, 25, 35, 35, 35}
	cellHeight := 15.0

	for i, header := range headers {
		pdf.CellFormat(cellWidths[i], cellHeight, header, "1", 0, "C", true, 0, "")
	}

	pdf.Ln(cellHeight)

	rows := strings.Split(values, ",")
	counter := 0

	pdf.SetFont("Arial", "", 10)

	for _, row := range rows {
		cells := strings.Split(row, ",")

		for _, cell := range cells {
			pdf.SetFillColor(255, 255, 255)

			pdf.CellFormat(cellWidths[counter], 10, cell, "1", 0, "C", false, 0, "")
			counter++
			if counter == 6 {
				pdf.Ln(10)
				counter = 0
			}

		}
	}

	w.Header().Set("Content-Disposition", "attachment; filename=generated_pdf.pdf")
	w.Header().Set("Content-Type", "application/pdf")
	if err := pdf.Output(w); err != nil {
		http.Error(w, "Failed to generate PDF", http.StatusInternalServerError)
	}
}
