package controller

import (
	"database/sql"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"

	_ "modernc.org/sqlite"
)

var db *sql.DB

type User struct {
	ID         int
	UserId     int
	Name       string
	Department string
	Shift      string
	Pin        string
	QR         string
	RFID       string
	Key        string
	Fprint     string
	Retinal    string
	Voice      string
}

func ViewHome(w http.ResponseWriter, r *http.Request) {

	tmpl, err := template.ParseFiles("./static/HTMLFiles/homepage.html")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func ViewUserHandler(w http.ResponseWriter, r *http.Request) {
	var err error
	db, err = sql.Open("sqlite", "SQLite_TAMS.db")
	if err != nil {
		log.Println("open Err: ", err)
	} else {
		log.Println("\nConnected to SQLite\n")
	}
	defer db.Close()

	rowValve := "SELECT * FROM userTable"
	allRow, err := db.Query(rowValve)
	if err != nil {
		panic(err.Error())
	}
	defer allRow.Close()

	userDatas := []User{}
	for allRow.Next() {
		var view User
		var pin, qr, rfid, key, fprint, retinal, voice sql.NullString

		err := allRow.Scan(&view.ID, &view.UserId, &view.Name, &view.Department, &view.Shift, &pin, &qr, &rfid, &key, &fprint, &retinal, &voice)
		if err != nil {
			panic(err.Error())
		}

		userDatas = append(userDatas, view)
	}

	data := struct {
		Users []User
	}{
		Users: userDatas,
	}

	tmpl, err := template.ParseFiles("./static/HTMLFiles/viewUser.html")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func CreateUserHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		userIdStr := r.FormValue("userId")
		name := r.FormValue("name")
		department := r.FormValue("department")
		shift := r.FormValue("shift")
		if userIdStr != "" || name != "" || department != "" || shift != "" {

			userId, err := strconv.Atoi(userIdStr)
			if err != nil {
				http.Error(w, "Invalid userId", http.StatusBadRequest)
				return
			}

			db, err := sql.Open("sqlite", "SQLite_TAMS.db")
			if err != nil {
				log.Println("open Err: ", err)
				http.Error(w, "Database connection error", http.StatusInternalServerError)
				return
			}
			defer db.Close()

			insertQuery := "INSERT INTO userTable (UserId, Name, Department, Shift) VALUES (?, ?, ?, ?)"
			_, err = db.Exec(insertQuery, userId, name, department, shift)
			if err != nil {
				log.Println("Insert error: ", err)
				http.Error(w, "Failed to insert user", http.StatusInternalServerError)
				return
			}

			http.Redirect(w, r, "/view", http.StatusFound)
		} else {
			fmt.Println("Invalid Entry")
		}
	} else {
		http.Error(w, "Invalid request method", http.StatusMethodNotAllowed)
	}
}

func UpdateUserHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		userID := r.FormValue("userId")
		userName := r.FormValue("userName")
		userDepartment := r.FormValue("userDepartment")
		userShift := r.FormValue("userShift")
		fmt.Printf("\nRow Data: %v, %v, %v, %v \n", userID, userName, userDepartment, userShift)

		db, err := sql.Open("sqlite", "SQLite_TAMS.db")
		if err != nil {
			log.Println("open Err: ", err)
			return
		} else {
			fmt.Println("\nConnected to SQLite\n")
		}
		defer db.Close()

		rowValve := "UPDATE userTable SET name = ?, department = ?, shift = ? WHERE userId = ? "
		_, err = db.Exec(rowValve, userName, userDepartment, userShift, userID)
		if err != nil {
			log.Println("Insert error: ", err)
			http.Error(w, "Failed to insert user", http.StatusInternalServerError)
			return
		}
		http.Redirect(w, r, "/view", http.StatusFound)
	}
}

func DeleteUserHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		userID := r.FormValue("delUserID")
		fmt.Printf("\nDelete UserId: %v \n", userID)

		db, err := sql.Open("sqlite", "SQLite_TAMS.db")
		if err != nil {
			log.Println("open Err: ", err)
			return
		} else {
			fmt.Println("\nConnected to SQLite\n")
		}
		defer db.Close()

		rowValve := "DELETE FROM userTable WHERE userId = ? "
		_, err = db.Exec(rowValve, userID)
		if err != nil {
			log.Println("Insert error: ", err)
			http.Error(w, "Failed to delete user", http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/view", http.StatusFound)
	}
}
