package controller

import (
	"database/sql"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"
	"time"

	_ "modernc.org/sqlite"
)

type Summary struct {
	ID           int
	Date         string
	UserId       int
	Name         string
	Timein       string
	Timeout      string
	MealIn       string
	MealOut      string
	BreakIn      string
	BreakOut     string
	OverTimeIn   string
	OverTimeOut  string
	HoursWork    float64
	OverTimeHour float64
}

type DataStruct struct {
	UserID       string
	Name         string
	AttendedDate string
	AttendedTime string
	Mode         string
}

func SummarizeViewHandler(w http.ResponseWriter, r *http.Request) {
	db, err := sql.Open("sqlite", "SQLite_TAMS.db")
	if err != nil {
		log.Println("open Err: ", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	defer db.Close()

	rowValve := `
		SELECT ut.userId, ut.name, at.attendedDate, at.attendedTime, at.mode
		FROM attendanceTable at
		INNER JOIN userTable ut ON at.userId = ut.userId
	`

	allRow, err := db.Query(rowValve)
	if err != nil {
		log.Println("allRow: ", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	defer allRow.Close()

	for allRow.Next() {
		var userId, name, attendedDate, attendedTime, mode string
		err := allRow.Scan(&userId, &name, &attendedDate, &attendedTime, &mode)
		if err != nil {
			log.Println("allRow_Scan: ", err)
			continue
		}

		var count int
		err = db.QueryRow("SELECT COUNT(*) FROM summaryTable WHERE date=? AND userid=?", attendedDate, userId).Scan(&count)
		if err != nil {
			log.Println("existsStmt_QuerryRow: ", err)
			continue
		}

		columnMap := map[string]string{
			"0": "timein",
			"1": "timeout",
			"2": "mealin",
			"3": "mealout",
			"4": "breakin",
			"5": "breakout",
			"6": "overtimein",
			"7": "overtimeout",
		}
		columnName, exists := columnMap[mode]
		data := DataStruct{
			UserID:       userId,
			Name:         name,
			AttendedDate: attendedDate,
			AttendedTime: attendedTime,
			Mode:         mode,
		}
		log.Println(data.UserID, data.Name, data.AttendedDate, data.AttendedTime)

		if count == 0 {
			if !exists {
				log.Println("Invalid mode")
				continue
			}

			query := fmt.Sprintf("SELECT userid, date, %s FROM summaryTable WHERE userid = ? AND date = ?", columnName)
			selectStmt, err := db.Prepare(query)
			if err != nil {
				log.Println("prepare Err: ", err)
				continue
			}
			defer selectStmt.Close()

			var existingUserID, existingDate, existingTime string
			err = selectStmt.QueryRow(data.UserID, data.AttendedDate).Scan(&existingUserID, &existingDate, &existingTime)
			if err != nil && err != sql.ErrNoRows {
				log.Println("select Err: ", err)
				continue
			}

			if existingUserID == "" && existingDate == "" {
				insertQuery := fmt.Sprintf("INSERT INTO summaryTable (userid, name, date, %s) VALUES (?, ?, ?, ?)", columnName)
				insertStmt, err := db.Prepare(insertQuery)
				if err != nil {
					log.Println("prepare Err: ", err)
					continue
				}
				defer insertStmt.Close()

				_, err = insertStmt.Exec(data.UserID, data.Name, data.AttendedDate, data.AttendedTime)
				if err != nil {
					log.Println("exec Err: ", err)
				}
				log.Println("Data inserted into summaryTable")
			}
		} else {

			err = updateSummaryTable(db, columnName, data.AttendedTime, data.UserID, data.AttendedDate)
			if err != nil {
				log.Println("updateSummaryTable error:", err)
			}
		}

		calculateAndUpdateHoursWork(db)
	}

	if err := allRow.Err(); err != nil {
		log.Println("Allrow_1: ", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// Fetch the data from the summaryTable for display in the table
	rows, err := db.Query("SELECT * FROM summaryTable ORDER BY date DESC")
	if err != nil {
		log.Println("Query summaryTable error:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	defer rows.Close()

	var summaryViews []Summary
	for rows.Next() {
		var summaryView Summary
		var nullableTimein, nullableTimeout, nullableMealin, nullableMealout, nullableBreakIn, nullableBreakOut, nullableOverTimeIn, nullableOverTimeOut sql.NullString
		var nullableHoursWork, nullableOverTimeHour sql.NullFloat64

		err := rows.Scan(
			&summaryView.ID,
			&summaryView.Date,
			&summaryView.UserId,
			&summaryView.Name,
			&nullableTimein,
			&nullableTimeout,
			&nullableMealin,
			&nullableMealout,
			&nullableBreakIn,
			&nullableBreakOut,
			&nullableOverTimeIn,
			&nullableOverTimeOut,
			&nullableHoursWork,
			&nullableOverTimeHour,
		)
		if err != nil {
			log.Println("rows.Scan error:", err)
			continue
		}

		summaryView.Timein = NullStringToString(nullableTimein)
		summaryView.Timeout = NullStringToString(nullableTimeout)
		summaryView.MealIn = NullStringToString(nullableMealin)
		summaryView.MealOut = NullStringToString(nullableMealout)
		summaryView.BreakIn = NullStringToString(nullableBreakIn)
		summaryView.BreakOut = NullStringToString(nullableBreakOut)
		summaryView.OverTimeIn = NullStringToString(nullableOverTimeIn)
		summaryView.OverTimeOut = NullStringToString(nullableOverTimeOut)

		if nullableHoursWork.Valid {
			summaryView.HoursWork = float64(nullableHoursWork.Float64)
		} else {
			summaryView.HoursWork = 0
		}

		if nullableOverTimeHour.Valid {
			summaryView.OverTimeHour = float64(nullableOverTimeHour.Float64)
		} else {
			summaryView.OverTimeHour = 0
		}

		summaryViews = append(summaryViews, summaryView)
	}

	pageStr := r.URL.Query().Get("page")
	page, _ := strconv.Atoi(pageStr)

	if page <= 0 {
		page = 1
	}

	viewsPerPage := 10
	startIndex := (page - 1) * viewsPerPage
	endIndex := startIndex + viewsPerPage

	var pagedViews []Summary
	if startIndex >= 0 && startIndex < len(summaryViews) {
		if endIndex > len(summaryViews) {
			endIndex = len(summaryViews)
		}
		pagedViews = summaryViews[startIndex:endIndex]
	}

	totalPages := len(summaryViews) / viewsPerPage
	if len(summaryViews)%viewsPerPage != 0 {
		totalPages++
	}

	pages := make([]int, totalPages)
	for i := 0; i < totalPages; i++ {
		pages[i] = i + 1
	}

	data := struct {
		Views      []Summary
		TotalPages int
		Pages      []int
	}{
		Views:      pagedViews,
		TotalPages: totalPages,
		Pages:      pages,
	}

	tmpl, err := template.ParseFiles("./static/HTMLFiles/summarizeTable.html")
	if err != nil {
		log.Println("template parse error:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, data)
	if err != nil {
		log.Println("template execute error:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

// For Null Datas
func NullStringToString(nullableString sql.NullString) string {
	return nullableString.String
}

// For Updating Datas
func updateSummaryTable(db *sql.DB, columnName, attendedTime, userID, attendedDate string) error {
	updateQuery := fmt.Sprintf("UPDATE summaryTable SET %s = ? WHERE userid = ? AND date = ?", columnName)
	updateStmt, err := db.Prepare(updateQuery)
	if err != nil {
		return err
	}
	defer updateStmt.Close()

	_, err = updateStmt.Exec(attendedTime, userID, attendedDate)
	if err != nil {
		return err
	}

	log.Println("Row updated in summaryTable")
	return nil
}

func SummarizeSearchHandler(w http.ResponseWriter, r *http.Request) {

	db, err := sql.Open("sqlite", "SQLite_TAMS.db")
	if err != nil {
		log.Println("open Err: ", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	defer db.Close()

	searchKey := "%" + r.FormValue("summarizeSearch") + "%"
	rowQuery := `
		SELECT * FROM summaryTable
		WHERE userId LIKE ? OR name LIKE ? OR date LIKE ?
		ORDER BY date DESC
	`
	allRows, err := db.Query(rowQuery, searchKey, searchKey, searchKey)
	if err != nil {
		log.Println("query Err: ", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	defer allRows.Close()

	summaryViews := make([]Summary, 0)
	for allRows.Next() {
		var summaryView Summary
		var nullableTimein, nullableTimeout, nullableMealin, nullableMealout, nullableBreakIn, nullableBreakOut, nullableOverTimeIn, nullableOverTimeOut sql.NullString
		var nullableHoursWork, nullableOverTimeHour sql.NullFloat64

		err := allRows.Scan(
			&summaryView.ID,
			&summaryView.Date,
			&summaryView.UserId,
			&summaryView.Name,
			&nullableTimein,
			&nullableTimeout,
			&nullableMealin,
			&nullableMealout,
			&nullableBreakIn,
			&nullableBreakOut,
			&nullableOverTimeIn,
			&nullableOverTimeOut,
			&nullableHoursWork,
			&nullableOverTimeHour,
		)
		if err != nil {
			log.Println("scan Err: ", err)
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		summaryView.Timein = NullStringToString(nullableTimein)
		summaryView.Timeout = NullStringToString(nullableTimeout)
		summaryView.MealIn = NullStringToString(nullableMealin)
		summaryView.MealOut = NullStringToString(nullableMealout)
		summaryView.BreakIn = NullStringToString(nullableBreakIn)
		summaryView.BreakOut = NullStringToString(nullableBreakOut)
		summaryView.OverTimeIn = NullStringToString(nullableOverTimeIn)
		summaryView.OverTimeOut = NullStringToString(nullableOverTimeOut)

		if nullableHoursWork.Valid {
			summaryView.HoursWork = float64(nullableHoursWork.Float64)
		} else {
			summaryView.HoursWork = 0
		}

		if nullableOverTimeHour.Valid {
			summaryView.OverTimeHour = float64(nullableOverTimeHour.Float64)
		} else {
			summaryView.OverTimeHour = 0
		}

		summaryViews = append(summaryViews, summaryView)
	}

	pageStr := r.URL.Query().Get("page")
	page, _ := strconv.Atoi(pageStr)

	if page <= 0 {
		page = 1
	}

	viewsPerPage := 10
	startIndex := (page - 1) * viewsPerPage
	endIndex := startIndex + viewsPerPage

	var pagedViews []Summary
	if startIndex >= 0 && startIndex < len(summaryViews) {
		if endIndex > len(summaryViews) {
			endIndex = len(summaryViews)
		}
		pagedViews = summaryViews[startIndex:endIndex]
	}

	totalPages := len(summaryViews) / viewsPerPage
	if len(summaryViews)%viewsPerPage != 0 {
		totalPages++
	}

	pages := make([]int, totalPages)
	for i := 0; i < totalPages; i++ {
		pages[i] = i + 1
	}

	data := struct {
		Views      []Summary
		TotalPages int
		Pages      []int
	}{
		Views:      pagedViews,
		TotalPages: totalPages,
		Pages:      pages,
	}

	tmpl, err := template.ParseFiles("./static/HTMLFiles/summarizeTable.html")
	if err != nil {
		log.Println("parseFiles Err: ", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, data)
	if err != nil {
		log.Println("execute Err: ", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

func calculateAndUpdateHoursWork(db *sql.DB) error {
	rows, err := db.Query("SELECT timeout, " +
		"timein FROM summaryTable")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var timeout, timein string
		err := rows.Scan(&timeout, &timein)
		if err != nil {
			return err
		}
		fmt.Printf("\nDate here: %v, %v \n", timeout, timein)

		hoursWorked := calculateHoursWorked(timeout, timein)
		hoursWorkFormatted := fmt.Sprintf("%.2f", hoursWorked)
		fmt.Println("formatted: ", hoursWorkFormatted)

		_, err = db.Exec("UPDATE summaryTable SET hourswork = ? "+
			"WHERE timeout = ? AND timein = ?",
			hoursWorkFormatted, timeout, timein)
		if err != nil {
			return err
		}
	}

	err = rows.Err()
	if err != nil {
		return err
	}

	return nil
}

func calculateHoursWorked(timeout, timein string) float64 {

	timeoutValue, err := time.Parse("15:04:05", timeout)
	if err != nil {
		log.Printf("Error parsing timeout value: %v", err)
		return 0
	}

	timeinValue, err := time.Parse("15:04:05", timein)
	if err != nil {
		log.Printf("Error parsing timein value: %v", err)
		return 0
	}
	//var duration float64
	duration := timeoutValue.Sub(timeinValue)
	//fmt.Printf("\nData Type: %T\n", duration)
	durationHours := duration.Hours()
	//durationValue := duration.Hours()
	//hoursWorked := int(math.Round(duration.Hours()))
	//fmt.Println("Here: ", durationValue)
	//fmt.Printf("duration type: %T\n", duration)
	//fmt.Printf("durationValue type: %T\n", durationValue)
	//fmt.Println(hoursWorked)
	//floatDuration := float64(durationValue)
	//fmt.Println("Here2: ", floatDuration)

	return durationHours
}

/*
   func ProcessData(data []DataStruct) {


   		db, err := sql.Open("sqlite", "SQLite_TAMS.db")
   		if err != nil {
   			log.Println("open Err: ", err)
   			return
   		}
   		defer db.Close()

   	insertStmt, err := db.Prepare("INSERT INTO summaryTable (userid, name, date, timein) VALUES (?, ?, ?, ?)")
   	if err != nil {
   		log.Println("prepare Err: ", err)
   		return
   	}
   	defer insertStmt.Close()

   	for _, d := range data {
   		_, err := insertStmt.Exec(d.UserID, d.Name, d.AttendedDate, d.AttendedTime, d.Mode)
   		if err != nil {
   			log.Println("exec Err: ", err)
   			continue
   		}

   		fmt.Printf("Data inserted to summaryTable:\n")
   		fmt.Printf("User ID: %s\n", d.UserID)
   		fmt.Printf("Name: %s\n", d.Name)
   		fmt.Printf("Attended Date: %s\n", d.AttendedDate)
   		fmt.Printf("Attended Time: %s\n", d.AttendedTime)
   		fmt.Printf("Attended Mode: %s\n", d.Mode)
   		fmt.Println("----------------------------------")
   	}
   }
*/
