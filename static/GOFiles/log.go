package controller

import (
	"database/sql"
	"fmt"
	"html/template"
	"log"
	_ "modernc.org/sqlite"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

type ViewData struct {
	CurrentTime string
	UTCRegion   string
}

type Response struct {
	Success bool   `json:"success"`
	Message string `json:"message,omitempty"`
	Data    []User `json:"data,omitempty"`
}

type User2 struct {
	UserId      int    `json:"userId"`
	Name        string `json:"name"`
	Pin         string `json:"pin"`
	QR          string `json:"qr"`
	RFID        string `json:"rfid"`
	Key         string `json:"key"`
	Fingerprint string `json:"fingerprint"`
	Retinal     string `json:"retinal"`
	Voice       string `json:"voice"`
}

func ViewLogsHandler(w http.ResponseWriter, r *http.Request) {

	tmpl, err := template.ParseFiles("./static/HTMLFiles/logUser.html")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func AttendedHandler(w http.ResponseWriter, r *http.Request) {
	var err error
	db, err = sql.Open("sqlite", "SQLite_TAMS.db")
	if err != nil {
		log.Println("open Err: ", err)
	}
	defer db.Close()

	methodEnter := r.FormValue("methodValue")
	modeEnter := r.FormValue("mode")
	fmt.Printf("\nReceived Mode: %v\n", modeEnter)
	modeInt, err := strconv.Atoi(modeEnter)

	if methodEnter != "" {
		rowValve := "SELECT userId, name, pin, qr, rfid, key, fprint, retinal, voice FROM userTable " +
			"WHERE pin = ? OR qr = ? OR rfid = ? OR key = ? OR fprint = ? OR retinal = ? OR voice = ?"
		allRow, err := db.Query(rowValve, methodEnter, methodEnter, methodEnter, methodEnter, methodEnter, methodEnter, methodEnter)
		if err != nil {
			log.Println("Query error:", err)
			http.Error(w, "Failed to retrieve user", http.StatusInternalServerError)
			return
		}
		defer allRow.Close()

		recordId := 0
		found := false
		fieldName := ""
		recordName := ""

		columns, err := allRow.Columns()
		if err != nil {
			log.Println("Error getting column names:", err)
			http.Error(w, "Failed to retrieve column names", http.StatusInternalServerError)
			return
		}

		values := make([]interface{}, len(columns))
		for i := range values {
			var value sql.NullString
			values[i] = &value
		}

		for allRow.Next() {
			err := allRow.Scan(values...)
			if err != nil {
				fmt.Println("Error scanning row:", err)
				continue
			}

			var foundFieldIndex, userIdIndex int

			for i, column := range columns {
				if column == "userId" {
					userIdIndex = i
				}

				fieldValue := values[i].(*sql.NullString)
				if fieldValue.Valid && methodEnter == fieldValue.String {
					fieldName = column
					foundFieldIndex = i
					break
				}
			}

			fmt.Printf("Value: %s, Field: %s, Name: %s UserId: %v \n", methodEnter, columns[foundFieldIndex], values[1].(*sql.NullString).String, values[userIdIndex].(*sql.NullString).String)

			found = true
			recordName = values[1].(*sql.NullString).String

			if userIdValue, ok := values[userIdIndex].(*sql.NullString); ok && userIdValue.Valid {
				recordId, err = strconv.Atoi(userIdValue.String)
				if err != nil {
					log.Println("Error converting userId to int:", err)
					http.Error(w, "Failed to convert userId", http.StatusInternalServerError)
					return
				}
			}
		}

		if !found {
			fmt.Println("Can't find data")
			errorMessage := "PIN not found. Please try again."
			http.Redirect(w, r, "/viewLogs?error2="+url.QueryEscape(errorMessage), http.StatusFound)
			return
		}

		currentTime := time.Now()
		UTCformat := time.Now().UTC()
		date := currentTime.Format("2006/01/02")
		t := currentTime.Format("15:04:05")

		fmt.Printf("\nUTCformat Format : %T\n", UTCformat)
		fmt.Printf("\nUTC Value: %v\n", UTCformat)
		fmt.Printf("\nDate Format Original: %T\n", currentTime)
		fmt.Println("Date:", date)
		fmt.Println("Time:", t)
		fmt.Printf("Date Format: %T\n", date)
		fmt.Printf("Date Format: %T\n", t)

		parsedDate, err := time.Parse("2006/01/02", date)
		if err != nil {
			fmt.Println("Error parsing date:", err)
			return
		}
		dateFloat := float64(parsedDate.Unix())
		fmt.Printf("Date Format Float: %v\n", parsedDate)
		fmt.Printf("Date Format Float: %T\n", parsedDate)
		fmt.Printf("Date Format Float: %v\n", dateFloat)
		fmt.Printf("Date Format Float: %T\n", dateFloat)

		insertQuery := "INSERT INTO attendanceTable (UserId, Name, Mode, AttendedDate, AttendedTime, Method) " +
			"VALUES (?, ?, ?, ?, ?, ?)"
		_, err = db.Exec(insertQuery, recordId, recordName, modeInt, date, t, fieldName)
		if err != nil {
			log.Println("Insert error: ", err)
			http.Error(w, "Failed to insert user", http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/viewLogs", http.StatusFound)
	}
}
