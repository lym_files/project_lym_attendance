

let selectedMode = '';

function modeInput(mode) {
    const inputElement = document.getElementById('mode');

    if (selectedMode !== mode) {
        inputElement.value = modeToString(mode);
        selectedMode = mode;
        console.log(mode)
    }
}

function modeToString(mode) {
    switch (mode) {
        case 0:
            return 0;
        case 1:
            return 1;
        case 2:
            return 2;
        case 3:
            return 3;
        case 4:
            return 4;
        case 5:
            return 5;
        case 6:
            return 6;
        case 7:
            return 7;
        default:
            return '';
    }
}
function pinInput(value) {
    document.getElementById('pin').value += value;
}

function updateTime() {
    var currentTimeElement = document.getElementById("currentTime");
    var currentTime = new Date().toLocaleTimeString();
    currentTimeElement.textContent = currentTime;
}
setInterval(updateTime, 1000);



