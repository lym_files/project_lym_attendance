
function getValues() {
    var values = [];
    var rows = document.getElementById("data-table").getElementsByTagName("tr");


    for (var i = 1; i < rows.length; i++) {
        var row = rows[i];
        var cells = row.getElementsByTagName("td");
        var rowValues = [];

        for (var j = 0; j < cells.length; j++) {
            var value = cells[j].innerText;
            rowValues.push(value);
        }

        values.push(rowValues);
    }
    alert(values)
    console.log("All Data:", values);


    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/attendPdf?all=" + values, true);
    console.log("To be sent", values)


    xhr.responseType = "blob";
    xhr.onload = function (event) {
        var blob = xhr.response;
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = "everything" + ".pdf";
        link.click();
    };
    xhr.send();
}

// Sample data
var data = []; // populate this array with your Go data

var table = document.getElementById("data-table");
var tbody = table.getElementsByTagName("tbody")[0];
var pagination = document.getElementById("pagination");

var rowsPerPage = 5; // number of rows to display per page
var currentPage = 1; // current page

// Function to display the data in the table
function displayData() {
    tbody.innerHTML = "";
    var startIndex = (currentPage - 1) * rowsPerPage;
    var endIndex = startIndex + rowsPerPage;

    for (var i = startIndex; i < endIndex && i < data.length; i++) {
        var row = document.createElement("tr");
        var userIdCell = document.createElement("td");
        var nameCell = document.createElement("td");
        var modeCell = document.createElement("td");
        var attendedDateCell = document.createElement("td");
        var attendedTimeCell = document.createElement("td");
        var methodCell = document.createElement("td");

        userIdCell.textContent = data[i].UserId;
        nameCell.textContent = data[i].Name;
        modeCell.textContent = data[i].Mode;
        attendedDateCell.textContent = data[i].AttendedDate;
        attendedTimeCell.textContent = data[i].AttendedTime;
        methodCell.textContent = data[i].Method;

        row.appendChild(userIdCell);
        row.appendChild(nameCell);
        row.appendChild(modeCell);
        row.appendChild(attendedDateCell);
        row.appendChild(attendedTimeCell);
        row.appendChild(methodCell);
        tbody.appendChild(row);
    }
}

// Function to generate pagination links
function generatePagination() {
    pagination.innerHTML = "";
    var totalPages = Math.ceil(data.length / rowsPerPage);

    for (var i = 1; i <= totalPages; i++) {
        var link = document.createElement("a");
        link.href = "javascript:void(0)";
        link.textContent = i;

        if (i === currentPage) {
            link.className = "active";
        }

        link.addEventListener("click", function () {
            currentPage = parseInt(this.textContent);
            displayData();
            generatePagination();
        });

        pagination.appendChild(link);
    }
}

// Initial display
displayData();
generatePagination();
