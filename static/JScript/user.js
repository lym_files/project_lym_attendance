var modal1 = document.getElementById("myModal1");
var closeBtn = document.getElementsByClassName("close")[0];

function updateModal(userId, name, department, shift) {

    console.log(userId)
    console.log(name)
    console.log(department)
    console.log(shift)
    document.getElementById("userIdInput").value = userId;
    document.getElementById("userName").value = name;
    document.getElementById("userDepart").value = department;
    document.getElementById("userShift").value = shift;
    modal1.style.display = "block";
}
function closeModal() {
    console.log("Close button clicked");
    modal1.style.display = "none";
}
closeBtn.onclick = function() {
    console.log("Close button event listener triggered");
    closeModal();
}
window.onclick = function(event) {
    console.log("Window click event listener triggered");
    if (event.target == modal1) {
        closeModal();
    }
}



var modal2 = document.getElementById("myModal2");
var closeBtn2 = document.getElementsByClassName("close")[0];


function deleteModal(userId) {
    document.getElementById("deleteUser").value = userId;
    modal2.style.display = "block";
    console.log("this is it", userId)
}

function closeModal() {
    modal2.style.display = "none";
}

closeBtn2.onclick = function() {
    closeModal();
};

window.onclick = function(event) {
    if (event.target == modal2) {
        closeModal();
    }
};


var modal3 = document.getElementById("myModal3");
var btn = document.getElementById("openModalBtn");
var span = document.getElementsByClassName("close")[0];

function openModal(userId) {
   console.log("registerModal: ",userId )
   var labelText = document.getElementById("labelText");
   labelText.innerHTML = "User ID: " + userId;
   modal3.style.display = "block";
}
span.onclick = function() {
   modal3.style.display = "none";
}
window.onclick = function(event) {
   if (event.target == modal3) {
       modal3.style.display = "none";
   }
}

