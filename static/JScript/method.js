document.addEventListener('DOMContentLoaded', function() {
    var inputrfid = document.getElementById("rfid");
    var inputpin = document.getElementById("pin");
    var inputmode = document.getElementById("mode");

    function updateInputFields() {
        if (inputmode.value === "") {
            inputrfid.disabled = true;
            inputpin.disabled = true;
        } else {
            inputrfid.disabled = false;
            inputpin.disabled = false;
        }
    }

    inputmode.addEventListener('input', updateInputFields);

    function sendData(value, mode) {
        const xhr = new XMLHttpRequest();
        const url = 'attended';
        const params = `methodValue=${encodeURIComponent(value)}&mode=${encodeURIComponent(mode)}`;
        xhr.open('POST', url, true);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.send(params);
        alert('Data sent successfully');
        window.location.reload();
    }

    function handleFormSubmission() {
        var valuerfid = inputrfid.value;
        var valuepin = inputpin.value;
        var valuemode = inputmode.value;

        if (valuemode === "") {
            inputrfid.addEventListener('input', updateInputFields);
            inputpin.addEventListener('input', updateInputFields);
        } else {
            inputrfid.removeEventListener('input', updateInputFields);
            inputpin.removeEventListener('input', updateInputFields);

            if (valuerfid && valuepin) {
                console.log("Please enter only one input (either RFID or PIN).");
                alert('Please enter only one input');
                window.location.reload();
                return;
            }

            if (valuerfid) {
                console.log("RFID:", valuerfid);
                console.log("Mode:", valuemode);
                sendData(valuerfid, valuemode);
            } else if (valuepin) {
                console.log("PIN:", valuepin);
                console.log("Mode:", valuemode);
                sendData(valuepin, valuemode);
            } else {
                console.log("No input provided.");
                window.location.reload();
            }

            const urlParams = new URLSearchParams(window.location.search);
            const error = urlParams.get('error');

            inputrfid.value = "";
            inputpin.value = "";
            inputmode.value = "";
        }
    }

    setTimeout(handleFormSubmission, 6000);
});


