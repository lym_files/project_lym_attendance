var table = document.getElementById("data-table");

for (var i = 1; i < table.rows.length; i++) {
    var timeinCell = table.rows[i].cells[3];
    var timeoutCell = table.rows[i].cells[4];
    var mealinCell = table.rows[i].cells[5];
    var mealoutCell = table.rows[i].cells[6];
    var overtimeinCell = table.rows[i].cells[9];
    var overtimeoutCell = table.rows[i].cells[10];
    var timein = timeinCell.innerHTML.trim();
    var timeout = timeoutCell.innerHTML.trim();
    var mealin = mealinCell.innerHTML.trim();
    var mealout = mealoutCell.innerHTML.trim();
    var overtimein = overtimeinCell.innerHTML.trim();
    var overtimeout = overtimeoutCell.innerHTML.trim();
    var hourswork = calculateHoursWork(timein, timeout);
    var overtimehour = calculateAdd(overtimein, overtimeout);
    var mealtimehour = calculateAdd(mealin, mealout);
    console.log("mealtimehour: ", mealtimehour);
    var totalhourswork = hourswork - mealtimehour;
    console.log("total: ", hourswork);
    console.log("total: ", mealtimehour);
    console.log("total: ", totalhourswork);
    var hoursworkCell = table.rows[i].cells[11];
    var hoursOverWorkCell = table.rows[i].cells[12];

    if (isNaN(totalhourswork)) {
        hoursworkCell.innerHTML = "0";
    } else {
        hoursworkCell.innerHTML = totalhourswork.toFixed(2);
    }

    if (isNaN(mealtimehour)) {
        hoursworkCell.innerHTML = hourswork.toFixed(2);
    }

    if (isNaN(overtimehour)) {
        hoursOverWorkCell.innerHTML = "0";
    } else {
        hoursOverWorkCell.innerHTML = overtimehour.toFixed(2);
    }
}


function calculateHoursWork(timein, timeout) {
    var timeinParts = timein.split(":");
    var timeoutParts = timeout.split(":");
    var timeinHours = parseInt(timeinParts[0]);
    var timeinMinutes = parseInt(timeinParts[1]);
    var timeoutHours = parseInt(timeoutParts[0]);
    var timeoutMinutes = parseInt(timeoutParts[1]);
    var totalMinutes = (timeoutHours * 60 + timeoutMinutes) - (timeinHours * 60 + timeinMinutes);
    var hours = Math.floor(totalMinutes / 60);
    var minutes = totalMinutes % 60;
    var hourswork = hours + minutes / 60; // Calculate as a decimal number
    return hourswork;
}

function calculateAdd(value1, value2) {
    var value1parts = value1.split(":");
    var value2parts = value2.split(":");
    var value1partsHours = parseInt(value1parts[0]);
    var value1partsMinutes = parseInt(value1parts[1]);
    var value2partsHours = parseInt(value2parts[0]);
    var value2partsminutes = parseInt(value2parts[1]);
    var totalMinutes = (value2partsHours * 60 + value2partsminutes) - (value1partsHours * 60 + value1partsMinutes);
    var hours = Math.floor(totalMinutes / 60);
    var minutes = totalMinutes % 60;
    var overtimehour = hours + minutes / 60; // Calculate as a decimal number
    return overtimehour;
}



function getValues() {
    var values = [];
    var rows = document.getElementById("data-table").getElementsByTagName("tr");


    for (var i = 1; i < rows.length; i++) {
        var row = rows[i];
        var cells = row.getElementsByTagName("td");
        var rowValues = [];

        for (var j = 0; j < cells.length; j++) {
            var value = cells[j].innerText;
            rowValues.push(value);
        }

        values.push(rowValues);
    }
    alert(values)
    console.log("All Data:", values);


    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/attendPdf?all=" + values, true);
    console.log("To be sent", values)


    xhr.responseType = "blob";
    xhr.onload = function (event) {
        var blob = xhr.response;
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = "everything" + ".pdf";
        link.click();
    };
    xhr.send();
}
